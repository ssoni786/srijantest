"""
    Exercise 3

    There is an attached log file from one of the development web server.

    Line number 2 of this log file is something like this "Processing by SprintsController#show as JSONAPI"

    This tells us, sprints controller's show action(method or function) ran. You have to parse this log file and output it the number of times the controller's actions run.

    Expected Output:

    SprintsController => show action ran 23 times
    SprintsController => index action ran 17 times
    and so on.


"""
import re
f = open('development.log', 'r')
data = f.read()
reg = re.compile(r'Processing by (\w*)#(\w*)')

parsed_data = reg.findall(data)
for e in sorted(set(parsed_data), key=lambda x:x[0]):
    action_count = len(filter(lambda r:r[0]==e[0] and r[1]==e[1], parsed_data))
    print '%s => %s action ran %d times' % (e[0], e[1], action_count)

#----------------OutOut------------------#
    # ApplicationController => login action ran 3 times
    # SprintsController => show action ran 22 times
    # SprintsController => index action ran 49 times
    # StoriesController => index action ran 8 times
    # StoriesController => update action ran 4 times
    # StoriesController => show action ran 62 times
    # StoryPointsController => create action ran 2 times
    # StoryPointsController => index action ran 3 times
    # UsersController => index action ran 43 times
#----------------------------------------#

